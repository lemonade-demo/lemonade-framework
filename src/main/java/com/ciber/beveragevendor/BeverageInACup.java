package com.ciber.beveragevendor;

import java.util.UUID;

import lombok.Data;

/***
 * Represents a serving of a beverage.
 * 
 * Includes a cup and a beverage, and the specified amount of contents.
 *
 */
@Data
public class BeverageInACup {
    String beverageServingID;
    final String product = "beverage serving";
    final String units = "size";
    String contents;
    String cupSize;
    int beverageAmount;
    int costCents;
    int priceCents;

    public BeverageInACup() {
        UUID uuid = UUID.randomUUID();
        this.beverageServingID = uuid.toString();
        this.costCents = 0;
        this.priceCents = 0;
    }

    public BeverageInACup(ServingCup cup, BeverageServing serving) {
        UUID uuid = UUID.randomUUID();
        this.beverageServingID = uuid.toString();
        this.cupSize = cup.size;
        this.beverageAmount = serving.amount;
        this.contents = serving.beverageType;
        this.costCents = serving.getCostCents() + cup.getCostCents();
        this.priceCents = serving.getCostCents() * 2 + cup.getCostCents() * 10;
    }
}
