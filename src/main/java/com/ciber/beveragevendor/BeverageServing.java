package com.ciber.beveragevendor;

import java.util.UUID;

import lombok.Data;

/***
 * Represents a serving of lemonade.
 *
 */
@Data
public class BeverageServing {
    String servingId;
    final String product = "Beverage";
    final String units = "ounces";
    String beverageType;
    int amount;
    int costCents;

    public BeverageServing() {
        UUID uuid = UUID.randomUUID();
        this.servingId = uuid.toString();
        this.costCents = 0;
    }

    public BeverageServing(String beverageType, int amount) {
        UUID uuid = UUID.randomUUID();
        this.servingId = uuid.toString();
        this.beverageType = beverageType;
        this.amount = amount;
        this.costCents = Math.max(1, amount / 2);
    }
}
