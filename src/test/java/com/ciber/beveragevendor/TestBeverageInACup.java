package com.ciber.beveragevendor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import org.junit.jupiter.api.Test;

class TestBeverageInACup {

    @Test
    void testBeverageCup() {
        BeverageInACup cup = new BeverageInACup();
        assertNotNull(cup);
    }

    @Test
    void testBeverageCupServingSmallCupBeverageServing() {
        BeverageServing serving = new BeverageServing("lemonade", 8);
        ServingCup cup = new ServingCup("small");

        BeverageInACup cupAndServing = new BeverageInACup(cup, serving);
        assertNotNull(cupAndServing);
        assertNotNull(cupAndServing.getBeverageServingID());
        assertEquals(8, cupAndServing.getBeverageAmount());
        assertEquals("lemonade", cupAndServing.getContents());
        assertEquals("small", cupAndServing.getCupSize());
        assertEquals(14, cupAndServing.getCostCents());
        assertEquals(108, cupAndServing.getPriceCents());
    }

    @Test
    void testBeverageCupServingMediumCupBeverageServing() {
        BeverageServing serving = new BeverageServing("lemonade", 8);
        ServingCup cup = new ServingCup("medium");

        BeverageInACup cupAndServing = new BeverageInACup(cup, serving);
        assertNotNull(cupAndServing);
        assertNotNull(cupAndServing.getBeverageServingID());
        assertEquals(8, cupAndServing.getBeverageAmount());
        assertEquals("lemonade", cupAndServing.getContents());
        assertEquals("medium", cupAndServing.getCupSize());
        assertEquals(14, cupAndServing.getCostCents());
        assertEquals(108, cupAndServing.getPriceCents());
    }

}
