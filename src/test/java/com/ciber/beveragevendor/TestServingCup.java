package com.ciber.beveragevendor;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
class TestServingCup {

    @BeforeAll
    static void Setup() {
        System.setProperty("log4j.configurationFile", "log4j.properties");
        // Logger LOGGER = LogManager.getLogger();
    }

    @Test
    void TestNothing() {

    }

    @Test
    void testNewServingCup() {
        ServingCup cup = new ServingCup();
        assertNotNull(cup);
    }

    @Test
    void testNewSmallServingCup() {
        ServingCup cup = new ServingCup("small");
        assertNotNull(cup);
        assertNotNull(cup.getCupId());
        log.info(cup.toString());
        assertEquals(8, cup.getOunces());
        assertEquals(10, cup.getCostCents());
    }

    @Test
    void testNewMediumServingCup() {
        ServingCup cup = new ServingCup("medium");
        assertNotNull(cup);
        assertNotNull(cup.getCupId());
        log.info(cup.toString());
        assertEquals(12, cup.getOunces());
    }

    @Test
    void testNewLargeServingCup() {
        ServingCup cup = new ServingCup("large");
        assertNotNull(cup);
        log.info(cup.toString());
        assertNotNull(cup.getCupId());
        assertEquals(16, cup.getOunces());
    }

    @Test
    void testNewIncorrectServingCup() {
        assertThrows(IllegalArgumentException.class, () -> {
            ServingCup cup = new ServingCup("bogus-size");
        });
    }

}
